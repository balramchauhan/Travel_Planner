﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TravelPlanner.Models
{
    public class Result
    {
        public bool Success { get; set; }
        public string Message { get; set; }
        public Object Res_Obj { get; set; }

        public Result(bool s, string m, Object o)
        {
            Success = s;
            Message = m;
            Res_Obj = o;
        }

        public Result(bool s, string m)
        {
            Success = s;
            Message = m;
        }
    }
}