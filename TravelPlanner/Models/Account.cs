﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Security;

namespace TravelPlanner.Models
{
    public class Account
    {
        public string Name { get; set; }
        public string Email { get; set; }
        public string Contact { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Status { get; set; }
        public int RouteId { get; set; }
        public int DetailsId { get; set; }
        public string Mode { get; set; }
        public string Agency { get; set; }
        public string Carrier { get; set; }
        public int Fare { get; set; }
        public string Source { get; set; }
        public string Destination { get; set; }
        public int BookingId { get; set; }

        public Result Booking(Account obj)
        {
            Result res = new Result(false, "Error while submitting form!");
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cons"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("Addbooking", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@name", obj.Name);
                        cmd.Parameters.AddWithValue("@email", obj.Email);
                        cmd.Parameters.AddWithValue("@contact",obj.Contact);
                        cmd.Parameters.AddWithValue("@date",obj.Date);
                        cmd.Parameters.AddWithValue("@time",obj.Time);
                        cmd.Parameters.AddWithValue("@mode",obj.Mode);
                        cmd.Parameters.AddWithValue("@source",obj.Source);
                        cmd.Parameters.AddWithValue("@destination",obj.Destination);
                        int i = Convert.ToInt32(cmd.ExecuteScalar());
                        if (i == -1)
                        {
                            res = new Result(false, "No Route between source and destination!");
                        }
                        else if (i == -2)
                        {
                            res = new Result(false, "Selected Travel Mode is not available between source and destination!");
                        }
                        else
                        {
                            res = new Result(true, "Details submitted successfully!");
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return res;
        }

        public Result BookingDetails(Account obj)
        {
            Result res = new Result(false, "Error while fetching details");
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cons"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("BookingDetails", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@email", obj.Email);
                        DataSet ds = new DataSet();
                        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                        sda.Fill(ds, "t");
                        if(ds.Tables["t"].Rows.Count>0)
                        {
                            List<Account> bookings = new List<Account>();
                            for(int i=0; i<ds.Tables["t"].Rows.Count;i++)
                            {
                                bookings.Add(new Account
                                {
                                    BookingId = int.Parse(ds.Tables["t"].Rows[i][0].ToString()),
                                    Name = ds.Tables["t"].Rows[i][1].ToString(),
                                    Email = ds.Tables["t"].Rows[i][2].ToString(),
                                    Contact = ds.Tables["t"].Rows[i][3].ToString(),
                                    Source = ds.Tables["t"].Rows[i][4].ToString(),
                                    Destination = ds.Tables["t"].Rows[i][5].ToString(),
                                    Mode = ds.Tables["t"].Rows[i][6].ToString(),
                                    Agency = ds.Tables["t"].Rows[i][7].ToString(),
                                    Carrier= ds.Tables["t"].Rows[i][8].ToString(),
                                    Fare = int.Parse(ds.Tables["t"].Rows[i][9].ToString()),
                                    Date = ds.Tables["t"].Rows[i][10].ToString(),
                                    Time = DateTime.Parse(ds.Tables["t"].Rows[i][11].ToString()).ToString("hh:mm tt"),
                                    Status = ds.Tables["t"].Rows[i][12].ToString()
                                });
                            }
                            res = new Result(true, "", bookings);
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return res;
        }

        public Result DeskView()
        {
            Result res = new Result(false, "Error while fetching details");
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cons"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("DeskView", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        DataSet ds = new DataSet();
                        SqlDataAdapter sda = new SqlDataAdapter(cmd);
                        sda.Fill(ds, "t");
                        if (ds.Tables["t"].Rows.Count > 0)
                        {
                            List<Account> bookings = new List<Account>();
                            for (int i = 0; i < ds.Tables["t"].Rows.Count; i++)
                            {
                                bookings.Add(new Account
                                {
                                    BookingId = int.Parse(ds.Tables["t"].Rows[i][0].ToString()),
                                    Name = ds.Tables["t"].Rows[i][1].ToString(),
                                    Email = ds.Tables["t"].Rows[i][2].ToString(),
                                    Contact = ds.Tables["t"].Rows[i][3].ToString(),
                                    Source = ds.Tables["t"].Rows[i][4].ToString(),
                                    Destination = ds.Tables["t"].Rows[i][5].ToString(),
                                    Mode = ds.Tables["t"].Rows[i][6].ToString(),
                                    Agency = ds.Tables["t"].Rows[i][7].ToString(),
                                    Carrier = ds.Tables["t"].Rows[i][8].ToString(),
                                    Fare = int.Parse(ds.Tables["t"].Rows[i][9].ToString()),
                                    Date = ds.Tables["t"].Rows[i][10].ToString(),
                                    Time = DateTime.Parse(ds.Tables["t"].Rows[i][11].ToString()).ToString("hh:mm tt"),
                                    Status = ds.Tables["t"].Rows[i][12].ToString()
                                });
                            }
                            res = new Result(true, "", bookings);
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return res;
        }

        public Result SetStatus(Account obj)
        {
            Result res = new Result(false, "Error while submitting form!");
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cons"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("setstatus", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@id", obj.BookingId);
                        cmd.Parameters.AddWithValue("@status", obj.Status);
                        int i = Convert.ToInt32(cmd.ExecuteScalar());
                        if (i == -1)
                        {
                            res = new Result(false, "No record modified!");
                        }
                        else
                        {
                            res = new Result(true, "");
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return res;
        }
    }

    public class TravelDesk
    {
        public string Email { get; set; }
        public string Password { get; set; }

        public Result AddDesk(TravelDesk obj)
        {
            Result res = new Result(false, "Error while adding!");
            obj.Password = Crypto.Hash(obj.Password);
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cons"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("AddDesk", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@email", obj.Email);
                        cmd.Parameters.AddWithValue("@pass", obj.Password);
                        int i = Convert.ToInt32(cmd.ExecuteScalar());
                        if (i == -1)
                        {
                            res = new Result(false, "User Already Exists!");
                        }
                        else
                        {
                            res = new Result(true, "Registered successfully! Now you can login.");
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }
            return res;
        }

        public Result DeskLogin(TravelDesk obj)
        {
            Result res = new Result(false, "Error while adding!");
            obj.Password = Crypto.Hash(obj.Password);
            try
            {
                using (SqlConnection con = new SqlConnection(ConfigurationManager.ConnectionStrings["cons"].ConnectionString))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand("DeskLogin", con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.AddWithValue("@email", obj.Email);
                        cmd.Parameters.AddWithValue("@pass", obj.Password);
                        int i = Convert.ToInt32(cmd.ExecuteScalar());
                        if (i == -1)
                        {
                            res = new Result(false, "User Not Exists! Please Register");
                        }
                        else
                        {
                            FormsAuthentication.SetAuthCookie(obj.Email, true);
                            res = new Account().DeskView();
                        }
                    }
                }
            }
            catch (Exception ex)
            {

            }
            return res;
        }
    }
    public class Crypto
    {
        public static string Hash(string value)
        {
            return Convert.ToBase64String(
                    System.Security.Cryptography.SHA256.Create()
                    .ComputeHash(Encoding.UTF8.GetBytes(value))
                    );

        }
    }
}