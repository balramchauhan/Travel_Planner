﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TravelPlanner.Models;

namespace TravelPlanner.Controllers
{
    public class AccountController : Controller
    {
        //
        // GET: /Account/
        public ActionResult Booking()
        {
            return View();
        }

        [HttpPost]
        public JsonResult Booking(Account obj)
        {
            Result res = obj.Booking(obj);
            return Json(res);
        }

        public ActionResult MyStatus()
        {
            return View();
        }

        [HttpPost]
        public JsonResult MyStatus(Account obj)
        {
            Result res = obj.BookingDetails(obj);
            return Json(res);
        }

        public ActionResult TravelDesk()
        {
            return View();
        }

        [HttpPost]
        public JsonResult DeskRegister(TravelDesk obj)
        {
            Result res = obj.AddDesk(obj);
            return Json(res);
        }

        [HttpPost]
        public JsonResult DeskLogin(TravelDesk obj)
        {
            Result res = obj.DeskLogin(obj);
            return Json(res);
        }

        [HttpPost]
        public JsonResult FetchDesk()
        {
            Result res = new Account().DeskView();
            return Json(res);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("TravelDesk");
        }

        [HttpPost]
        public JsonResult SetStatus(Account obj)
        {
            Result res = obj.SetStatus(obj);
            return Json(res);
        }
	}
}