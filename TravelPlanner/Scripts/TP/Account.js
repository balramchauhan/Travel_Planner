﻿
function Booking() {
    var msg = validateform();
    if (msg.length > 0) {
        swal('Oops..', msg, 'error');
        return false;
    }
    else {
        var obj = new Object();
        obj.Name = $('#txt_name').val();
        obj.Email = $('#txt_email').val();
        obj.Contact = $('#txt_contact').val();
        obj.Mode = $('#txt_mode').val();
        obj.Date = $('#txt_date').val();
        obj.Time = $('#txt_time').val();
        obj.Source = $('#txt_source').val();
        obj.Destination = $('#txt_destination').val();
        datatosend = '{"obj":' + JSON.stringify(obj) + '}';

        var _success = function (response) {
            if(response.Success==false)
            {
                swal('Oops..', response.Message, 'error');
            }
            else
            {
                swal('Great', response.Message, 'success');
            }
        }
        var _error = function (response) {
            swal('Oops..', 'Error While submitting form!', 'error');
        }
        CallAjaxJson('/Account/Booking', datatosend, _success, _error);
    }
}


function validEmail(email)
{
    var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    return expr.test(email);
};

function validContact(ContactNumber) {
    var expr = /^(^[7-9]\d{9}$)/;
    return expr.test(ContactNumber);
}

function validateform() {
    var msg = new String();
    if (!!$('#txt_name').val()) { } else { msg += "Name is required!"; return msg; }
    if (!validEmail($('#txt_email').val())) { msg += "Invalid Email Address!"; return msg; }
    if (!validContact($('#txt_contact').val())) { msg += "Invalid Contact Number!"; return msg; }
    if ($('#txt_mode').val() == 'Select') { msg += "Select Travel Mode!"; return msg; }
    if (!!$('#txt_date').val()) { } else { msg += "Select Date!"; return msg; }
    if (!!$('#txt_time').val()) { } else { msg += "Select Time!"; return msg; }
    if ($('#txt_source').val() == 'Select') { msg += "Select Source Station!"; return msg; }
    if ($('#txt_destination').val() == 'Select') { msg += "Select Destination Station"; return msg; }
    return msg;
}

function BookingDetails() {
    if (!validEmail($('#txt_email').val())) { swal('Oops..', 'Email Address Invalid!', 'error'); }
    else {
        var obj = new Object();
        obj.Email = $('#txt_email').val();
        datatosend = '{"obj":' + JSON.stringify(obj) + '}';

        var _success = function (response) {
            if (response.Success == false) {
                swal('Oops..', response.Message, 'error');
            }
            else {
                var data = response.Res_Obj;
                var trHTML = '<thead><tr><th style="text-align:center">Name</th><th style="text-align:center">Email</th><th style="text-align:center">Contact</th><th style="text-align:center">Source</th><th style="text-align:center">Destination</th><th style="text-align:center">Mode</th><th style="text-align:center">Agency</th><th style="text-align:center">Carrier</th><th style="text-align:center">Fare</th><th style="text-align:center">Date</th><th style="text-align:center">Time</th><th style="text-align:center">Status</th></tr></thead><tbody>';
                $.each(data, function (i, item) {
                    trHTML += '<tr><td>' + item.Name + '</td><td>' + item.Email + '</td><td>' + item.Contact + '</td><td>'+item.Source+'</td><td>'+item.Destination+'</td><td>'+item.Mode+'</td><td>'+item.Agency+'</td><td>'+item.Carrier+'</td><td>'+item.Fare+'</td><td>'+item.Date+'</td><td>'+item.Time+'</td><td>'+item.Status+'</td></tr>';
                });
                trHTML += '</tbody>';
                $('#Booking_Status_Table').html(trHTML);
                $('#Booking_Status_Table').DataTable();
            }
        }
        var _error = function (response) {
            swal('Oops..', 'Error While getting data!', 'error');
        }
        CallAjaxJson('/Account/MyStatus', datatosend, _success, _error);
    }
}

function Register() {
    if (!validEmail($('#txt_email').val())) { swal('Oops..', 'Invalid Email Address!', 'error'); return false; }
    else if (!!$('#txt_pass').val()) { if ($('#txt_pass').val() != $('#txt_pass_conf').val()) { swal('Oops..', 'Password missmatch!', 'error'); return false; } } else { swal('Oops..', 'Password Required!', 'error'); return false; }
    var obj = new Object();
    obj.Email = $('#txt_email').val();
    obj.Password = $('#txt_pass').val();
    datatosend = '{"obj":' + JSON.stringify(obj) + '}';

    var _success = function (response) {
        if (response.Success == false) {
            swal('Oops..', response.Message, 'error');
        }
        else {
            swal('Great', response.Message, 'success');
        }
    }
    var _error = function (response) {
        swal('Oops..', 'Error While submitting form!', 'error');
    }
    CallAjaxJson('/Account/DeskRegister', datatosend, _success, _error);
}

function LoginUser() {
    if (!validEmail($('#txt_email_log').val())) { swal('Oops..', 'Invalid Email Address!', 'error'); return false; }
    else if (!!$('#txt_pass_log').val()) { } else { swal('Oops..', 'Password Required!', 'error'); return false; }
    var obj = new Object();
    obj.Email = $('#txt_email_log').val();
    obj.Password = $('#txt_pass_log').val();
    datatosend = '{"obj":' + JSON.stringify(obj) + '}';

    var _success = function (response) {
        if (response.Success == false) {
            swal('Oops..', response.Message, 'error');
        }
        else {
            var data = response.Res_Obj;
            var trHTML = '<thead><tr><th style="text-align:center">Booking Id</th><th style="text-align:center">Name</th><th style="text-align:center">Email</th><th style="text-align:center">Contact</th><th style="text-align:center">Source</th><th style="text-align:center">Destination</th><th style="text-align:center">Mode</th><th style="text-align:center">Agency</th><th style="text-align:center">Carrier</th><th style="text-align:center">Fare</th><th style="text-align:center">Date</th><th style="text-align:center">Time</th><th style="text-align:center">Status</th></tr></thead><tbody>';
            $.each(data, function (i, item) {
                trHTML += '<tr><td>' + item.BookingId + '</td><td>' + item.Name + '</td><td>' + item.Email + '</td><td>' + item.Contact + '</td><td>' + item.Source + '</td><td>' + item.Destination + '</td><td>' + item.Mode + '</td><td>' + item.Agency + '</td><td>' + item.Carrier + '</td><td>' + item.Fare + '</td><td>' + item.Date + '</td><td>' + item.Time + '</td><td><a onclick="changestatus('+item.BookingId+')">'+item.Status+'</a></td></tr>';
            });
            trHTML += '</tbody>';
            $('#Travel_Desk_Table').html(trHTML);
            $('#Travel_Desk_Table').DataTable();
            $('#Login').css('display', 'none');
            $('#SignUp').css('display', 'none');
            $('#LoginDiv').css('display', 'none');
            $('#SignupDiv').css('display', 'none');
        }
    }
    var _error = function (response) {
        swal('Oops..', 'Error While submitting form!', 'error');
    }
    CallAjaxJson('/Account/DeskLogin', datatosend, _success, _error);
}

function fetchDesk()
{
    datatosend = '';

    var _success = function (response) {
        if (response.Success == false) {
            swal('Oops..', response.Message, 'error');
        }
        else {
            var data = response.Res_Obj;
            var trHTML = '<thead><tr><th style="text-align:center">Booking Id</th><th style="text-align:center">Name</th><th style="text-align:center">Email</th><th style="text-align:center">Contact</th><th style="text-align:center">Source</th><th style="text-align:center">Destination</th><th style="text-align:center">Mode</th><th style="text-align:center">Agency</th><th style="text-align:center">Carrier</th><th style="text-align:center">Fare</th><th style="text-align:center">Date</th><th style="text-align:center">Time</th><th style="text-align:center">Status</th></tr></thead><tbody>';
            $.each(data, function (i, item) {
                trHTML += '<tr><td>' + item.BookingId + '</td><td>' + item.Name + '</td><td>' + item.Email + '</td><td>' + item.Contact + '</td><td>' + item.Source + '</td><td>' + item.Destination + '</td><td>' + item.Mode + '</td><td>' + item.Agency + '</td><td>' + item.Carrier + '</td><td>' + item.Fare + '</td><td>' + item.Date + '</td><td>' + item.Time + '</td><td><a onclick="changestatus('+item.BookingId+')">'+item.Status+'</a></td></tr>';
            });
            trHTML += '</tbody>';
            $('#Travel_Desk_Table').html(trHTML);
            $('#Travel_Desk_Table').DataTable();
            $('#Login').css('display', 'none');
            $('#SignUp').css('display', 'none');
            $('#LoginDiv').css('display', 'none');
            $('#SignupDiv').css('display', 'none');
        }
    }
    var _error = function (response) {
        swal('Oops..', 'Error While submitting form!', 'error');
    }
    CallAjaxJson('/Account/FetchDesk', datatosend, _success, _error);
}

function changestatus(bookingId) {
    $('#bookingId').val(bookingId);
    $('#StatusModal').modal("show");
}

function SetStatus() {
    var obj = new Object();
    obj.BookingId = $('#bookingId').val();
    obj.Status = $('#book_status').val();
    datatosend = '{"obj":' + JSON.stringify(obj) + '}';
    var _success = function (response) {
        if (response.Success == false) {
            swal('Oops..', response.Message, 'error');
            $('#StatusModal').modal("hide");
        }
        else {
            swal('Great..', response.Message, 'success');
            $('#StatusModal').modal("hide");
            fetchDesk();
        }
    }
    var _error = function (response) {
        swal('Oops..', 'Error While submitting form!', 'error');
    }
    CallAjaxJson('/Account/SetStatus', datatosend, _success, _error);
}