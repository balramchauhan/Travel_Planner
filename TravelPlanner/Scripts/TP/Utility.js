﻿function CallAjaxJson(url, datatosend, _success, _error) {
    var _logouthandler = function (response) {
        if (typeof (response.sessiontimeout) != 'undefined') {
            swal('Oops', 'Your session has timed out. Please login and retry.', 'error');
            window.location.href = "/";
            return;
        }
        else {
            _success(response);
        }
    };
    $.ajax({
        type: 'POST',
        url: url,
        data: datatosend,
        dataType: 'json',
        contentType: 'Application/json;charset=utf-8',
        error: _error,
        success: _logouthandler
    });
}